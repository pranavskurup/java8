package bluez.codez.demo.java8.lamda;

import bluez.codez.demo.java8.Person;

import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Built-in Functional Interfaces
 * The JDK 1.8 API contains many built-in functional interfaces.
 * Some of them are well known from older versions of Java like
 * Comparator or Runnable. Those existing interfaces are extended to enable
 * Lambda support via the @FunctionalInterface annotation.
 * <p>
 * <p>
 * Created by pranav on 12/30/2014.
 */
public class Lambda3 {

    public static void main(String[] args) throws Exception {
        /**Predicates
         * Predicates are boolean-valued functions of one argument.
         * The interface contains various default methods for composing
         * predicates to complex logical terms (and, or, negate)*/

        Predicate<String> predicate = (s) -> s.length() > 0;

        predicate.test("foo");              // true
        predicate.negate().test("foo");     // false

        Predicate<Boolean> nonNull = Objects::nonNull;
        Predicate<Boolean> isNull = Objects::isNull;

        Predicate<String> isEmpty = String::isEmpty;
        Predicate<String> isNotEmpty = isEmpty.negate();

        /** Functions
         * Functions accept one argument and produce a result.
         * Default methods can be used to chain multiple functions together (compose, andThen).*/

        Function<String, Integer> toInteger = Integer::valueOf;
        Function<String, String> backToString = toInteger.andThen(String::valueOf);

        backToString.apply("123");     // "123"

        /**Suppliers
         * Suppliers produce a result of a given generic type. Unlike Functions, Suppliers don't accept arguments.*/


        Supplier<Person> personSupplier = Person::new;
        personSupplier.get();   // new Person

        /**Consumers
         * Consumers represents operations to be performed on a single input argument.*/
        Consumer<Person> greeter = (p) -> System.out.println("Hello, " + p.getFirstName());

        greeter.accept(new Person("Luke", "Skywalker"));


        /**Comparators
         * Comparators are well known from older versions of Java. Java 8 adds various default methods to the interface.*/

        Comparator<Person> comparator = (p1, p2) -> p1.getFirstName().compareTo(p2.getFirstName());

        Person p1 = new Person("John", "Doe");
        Person p2 = new Person("Alice", "Wonderland");

        System.out.println(comparator.compare(p1, p2));                 // > 0
        System.out.println(comparator.reversed().compare(p1, p2));      // < 0


        /**Optionals
         * Optionals are not functional interfaces, instead it's a nifty utility to prevent NullPointerException.
         *
         * Optional is a simple container for a value which may be null or non-null.
         * Think of a method which may return a non-null result but sometimes return nothing.
         * Instead of returning null you return an Optional in Java 8.*/

        Optional<String> optional = Optional.of("bam");

        optional.isPresent();           // true
        optional.get();                 // "bam"
        optional.orElse("fallback");    // "bam"

        optional.ifPresent((s) -> System.out.println(s.charAt(0)));     // "b"


        // Runnables

        Runnable runnable = () -> System.out.println(UUID.randomUUID());
        runnable.run();


        // Callables

        Callable<UUID> callable = () -> UUID.randomUUID();
        UUID call = callable.call();
        System.out.println(call);
        call = callable.call();
        System.out.println(call);
        call = callable.call();
        System.out.println(call);
    }

    @FunctionalInterface
    interface Fun {
        void foo();
    }
}
