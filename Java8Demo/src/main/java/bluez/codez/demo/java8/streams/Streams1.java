package bluez.codez.demo.java8.streams;

import java.util.ArrayList;
import java.util.List;

/**
 * A java.util.Stream represents a sequence of elements on which one or more operations can be performed.
 * Stream operations are either intermediate or terminal. While terminal operations return a result of a certain type,
 * intermediate operations return the stream itself so you can chain multiple method calls in a row.
 * Streams are created on a source, e.g. a java.util.Collection like lists or sets (maps are not supported).
 * Stream operations can either be executed sequential or parallel.
 * <p>
 * Created by pranav on 12/30/2014.
 */
public class Streams1 {

    /**
     * Collections in Java 8 are extended so you can simply create streams either
     * by calling Collection.stream() or Collection.parallelStream()..
     */
    public static void main(String[] args) {

        List<String> stringCollection = new ArrayList<>();
        stringCollection.add("ddd2");
        stringCollection.add("aaa2");
        stringCollection.add("bbb1");
        stringCollection.add("aaa1");
        stringCollection.add("bbb3");
        stringCollection.add("ccc");
        stringCollection.add("bbb2");
        stringCollection.add("ddd1");


        /**Filter
         * Filter accepts a predicate to filter all elements of the stream.
         * This operation is intermediate which enables us to call another stream operation (forEach) on the result.
         * ForEach accepts a consumer to be executed for each element in the filtered stream.
         * ForEach is a terminal operation. It's void, so we cannot call another stream operation.*/

        stringCollection
                .stream()
                .filter((s) -> s.startsWith("a"))
                .forEach(System.out::println);

        /**Sorted
         * Sorted is an intermediate operation which returns a sorted view of the stream.
         * The elements are sorted in natural order unless you pass a custom Comparator.*/

        stringCollection
                .stream()
                .sorted()
                .filter((s) -> s.startsWith("a"))
                .forEach(System.out::println);


        /**Map
         * The intermediate operation map converts each element into another object via the given function.
         * The following example converts each string into an upper-cased string.
         * But we can also use map to transform each object into another type.
         * The generic type of the resulting stream depends on the generic type of the function you pass to map.*/

        stringCollection
                .stream()
                .map(String::toUpperCase)
                .sorted((a, b) -> b.compareTo(a))
                .forEach(System.out::println);

    }
}
